import { Component, OnInit } from '@angular/core';

import { ImageUploadService } from '../../services/image-upload.service';


@Component({
	selector: 'app-image-upload',
	templateUrl: './image-upload.component.html',
	styleUrls: ['./image-upload.component.css']
})
export class ImageUploadComponent implements OnInit {
	public selectedFile: File;
	constructor() {}
	ngOnInit() {}

	onFileChanged(event: any): void {
		this.selectedFile = event.target.files[0];
	}
}

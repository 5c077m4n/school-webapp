import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material';

import { SchoolService } from '../../services/school.service';
import { Admin } from '../../models/admin';
import { SharedMessengerService } from '../../services/shared-messenger.service';
import { ImageUploadService } from '../../services/image-upload.service';
import { SnackBarService } from '../../services/snack-bar.service';


@Component({
	selector: 'app-admin-main',
	templateUrl: './admin-main.component.html',
	styleUrls: ['./admin-main.component.css']
})
export class AdminMainComponent implements OnInit {
	public admins: Admin[] = [];
	private listener: Subscription;
	constructor(
		private bottomSheet: MatBottomSheet,
		private schoolService: SchoolService,
		private messenger: SharedMessengerService
	) {}
	ngOnInit() {
		this.getAllAdmins();
		this.listener = this.messenger.getObservable()
		.subscribe(msg => (msg === 'updateAdmins')? this.getAllAdmins() : undefined);
	}
	private getAllAdmins(): void {
		this.schoolService.getAllAdmins().subscribe(
			response => this.admins = (response)? response : []
		);
	}
	public openEditAdminPage(admin: Admin) {
		this.openBottomSheet();
		this.messenger.emit(admin);
	}
	private openBottomSheet(): void {
		this.bottomSheet.open(AddAdminBottomSheetComponent);
	}

	ngOnDestroy() {
		this.listener.unsubscribe();
	}
}


@Component({
	templateUrl: './add-admin-bottom-sheet.component.html',
})
export class AddAdminBottomSheetComponent implements OnInit, OnDestroy {
	public adminInEditing: Admin = {};
	private listener: Subscription;
	constructor(
		private bottomSheetRef: MatBottomSheetRef<AddAdminBottomSheetComponent>,
		private schoolService: SchoolService,
		private imageUploadService: ImageUploadService,
		private snackBar: SnackBarService,
		private messenger: SharedMessengerService
	) {}
	ngOnInit() {
		this.listener = this.messenger.getObservable()
		.subscribe(msg => this.adminInEditing = (msg.role)? msg : {});
	}

	addAdmin(hp: string, pwd2:string, imageFile: File): void {
		if(hp && hp.length > 0) return;
		if(
			!this.adminInEditing.name ||
			!this.adminInEditing.email ||
			(this.adminInEditing.password !== pwd2.trim()) ||
			!this.adminInEditing.role
		) return;
		if(!imageFile) {
			this.schoolService.addAdmin(
				this.adminInEditing.name.trim(),
				this.adminInEditing.role,
				this.adminInEditing.phone,
				this.adminInEditing.email.trim(),
				null,
				this.adminInEditing.password
			)
			.subscribe(response => {
				if(!response) return;
				this.bottomSheetRef.dismiss();
				this.messenger.emit('updateAdmins');
				this.snackBar.show(`New student added :D`);
			});
		}
		else {
			if(imageFile.size > 4e6) {
				this.snackBar.show('Please choose a smaller image (less than 4MB).');
				return;
			}
			this.imageUploadService.uploadImage(imageFile)
			.subscribe(response => {
				if(response && response.link) {
					this.schoolService
					.addAdmin(
						this.adminInEditing.name.trim(),
						this.adminInEditing.role,
						this.adminInEditing.phone,
						this.adminInEditing.email.trim(),
						response.link,
						this.adminInEditing.password
					)
					.subscribe(data => {
						if(!response) return;
						this.bottomSheetRef.dismiss();
						this.messenger.emit('updateAdmins');
						this.snackBar.show(`New student added :D`);
					});
				}
			});
		}
	}
	public deleteAdmin(id: number): void {
		this.schoolService.deleteAdmin(id).subscribe(
			data => {
				if(!data) return;
				this.bottomSheetRef.dismiss();
				this.messenger.emit('updateAdmins');
				this.snackBar.show(`The course has been deleted :O`);
			}
		);
	}

	ngOnDestroy() {
		this.listener.unsubscribe();
	}
}

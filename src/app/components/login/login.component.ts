import { Component, OnInit } from '@angular/core';

import { SchoolAuthService } from '../../services/school-auth.service';
import { SnackBarService } from '../../services/snack-bar.service';
import { LocalStorageService } from '../../services/local-storage.service';


@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	constructor(
		private schoolAuthService: SchoolAuthService,
		private storageService: LocalStorageService,
		private snackBar: SnackBarService
	) {}
	ngOnInit() {}

	login(hp: string, email: string, password: string): void {
		if(hp && hp.length > 0) return;
		email = email.trim();
		password = password.trim();
		if(!email || !password) return;
		else this.schoolAuthService.login(email, password).subscribe(
			data => {
				if(!data) return;
				data.email = email;
				data.timestamp = Date.now();
				this.storageService.add('currentUser', data);
				this.snackBar.show('Welcome!');
			}
		);
	}
	logout(): void {
		this.schoolAuthService.logout();
		this.snackBar.show('See you later!');
	}
}

export interface LaravelResponse<T> {
	data: T;
    links?: {
        first?: string;
        last?: string;
        prev?: string;
        next?: string;
    },
    meta?: {
        current_page?: number;
        from?: string;
        last_page?: number;
        path?: string;
        per_page?: number;
        to?: number;
        total?: number;
	}
	error?: {
		error?: string;
	};
}

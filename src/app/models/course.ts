import { Student } from "./student";


export interface Course {
	id?: number;
	name?: string;
	description?: string;
	image_url?: string;
	students?: Student[];
}

import { Component, OnInit } from '@angular/core';

import { ImageUploadService } from '../../services/image-upload.service';
import { SnackBarService } from '../../services/snack-bar.service';
import { SchoolAuthService } from '../../services/school-auth.service';
import { LocalStorageService } from '../../services/local-storage.service';


@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
	constructor(
		private schoolAuthService: SchoolAuthService,
		private storgeService: LocalStorageService,
		private imageUploadService: ImageUploadService,
		private snackBar: SnackBarService
	) {}
	ngOnInit() {}

	register(...args: any[]): void {
		if(args[0] && args[0].length > 0) return;
		let [_, name, role, phone, email, imageFile, password, password2]: any[] = args;
		if(!email.trim() || !password.trim() || (password.trim() !== password2)) return;
		if(!imageFile) {
			this.schoolAuthService
			.register(name.trim(), role, phone.trim(), email.trim(), null, password)
			.subscribe(data => {
				if(data) {
					data.email = email.trim();
					data.timestamp = Date.now();
					this.storgeService.add('currentUser', data);
					this.snackBar.show(`Nice to meet you, ${name} :D`);
				}
			});
		}
		else {
			if(imageFile.size > 4e6) {
				this.snackBar.show('Please choose a smaller image (less than 4MB).');
				return;
			}
			this.imageUploadService.uploadImage(imageFile)
			.subscribe(response => {
				if(response && response.link) {
					this.schoolAuthService
					.register(
						name.trim(),
						role,
						phone.trim(),
						email.trim(),
						response.link,
						password.trim()
					)
					.subscribe(data => {
						if(!data) return;
						data.email = email;
						data.timestamp = Date.now();
						this.storgeService.add('currentUser', data);
						this.snackBar.show(`Nice to meet you, ${name} :D`);
					});
				}
			});
		}
	}
}

import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { SchoolService } from '../../services/school.service';
import { SharedMessengerService } from '../../services/shared-messenger.service';
import { SnackBarService } from '../../services/snack-bar.service';
import { ImageUploadService } from '../../services/image-upload.service';
import { Course } from '../../models/course';
import { Student } from '../../models/student';


@Component({
	selector: 'app-course-list',
	templateUrl: './course-list.component.html',
	styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit, OnDestroy {
	public courses: Course[];
	private listener: Subscription;
	constructor(
		private bottomSheet: MatBottomSheet,
		private schoolService: SchoolService,
		private messenger: SharedMessengerService
	) {}
	ngOnInit() {
		this.getAllCourses();
		this.listener = this.messenger.getObservable()
		.subscribe(msg => (msg === 'refreshLists')? this.getAllCourses() : undefined);
	}
	public getAllCourses(): void {
		this.schoolService.getAllCourses().subscribe(
			response => this.courses = (response)? response : []
		);
	}
	public arrToStr(arr: any[]): string {
		let strOut: string = '';
		arr.forEach(item => strOut += `${item.name}, `);
		return strOut;
	}
	public openEditCoursePage(course: Course) {
		this.openBottomSheet();
		this.messenger.emit(course);
	}
	public openBottomSheet(): void {
		this.bottomSheet.open(AddCourseBottomSheetComponent);
	}
	ngOnDestroy() {
		this.listener.unsubscribe();
	}
}


@Component({
	templateUrl: './add-course-bottom-page.component.html'
})
export class AddCourseBottomSheetComponent implements OnInit, OnDestroy {
	public courseInEditing: Course = {};
	private listener: Subscription;
	constructor(
		private bottomSheetRef: MatBottomSheetRef<AddCourseBottomSheetComponent>,
		private schoolService: SchoolService,
		private imageUploadService: ImageUploadService,
		private snackBar: SnackBarService,
		private messenger: SharedMessengerService
	) {}
	ngOnInit() {
		this.listener = this.messenger.getObservable()
		.subscribe(msg => this.courseInEditing = (msg.students)? msg : {});
	}

	public addCourse(imageFile?: File): void {
		if(!this.courseInEditing.name.trim()) return;
		if(!imageFile) {
			this.schoolService.addCourse(
				this.courseInEditing.name, this.courseInEditing.description, null
			).subscribe(response => {
				if(!response) return;
				this.bottomSheetRef.dismiss();
				this.messenger.emit('refreshLists');
				this.snackBar.show(`New course added :D`);
			});
		}
		else {
			if(imageFile.size > 4e6) {
				this.snackBar.show('Please choose a smaller image (less than 4MB).');
				return;
			}
			this.imageUploadService.uploadImage(imageFile)
			.subscribe(response => {
				if(response && response.link) {
					this.schoolService
					.addCourse(
						this.courseInEditing.name,
						this.courseInEditing.description,
						response.link
					)
					.subscribe(data => {
						if(!data) return;
						this.bottomSheetRef.dismiss();
						this.messenger.emit('refreshLists');
						this.snackBar.show(`New course added :D`);
					});
				}
			});
		}
	}
	public updateCourse(imageFile?: File): void {
		if(!this.courseInEditing.name.trim()) return;
		if(!imageFile) {
			this.schoolService.editCourse(
				this.courseInEditing.id,
				this.courseInEditing.name,
				this.courseInEditing.description,
				null
			).subscribe(response => {
				if(!response) return;
				this.bottomSheetRef.dismiss();
				this.messenger.emit('refreshLists');
				this.snackBar.show(`New course added :D`);
			});
		}
		else {
			if(imageFile.size > 4e6) {
				this.snackBar.show('Please choose a smaller image (less than 4MB).');
				return;
			}
			this.imageUploadService.uploadImage(imageFile)
			.subscribe(response => {
				if(response && response.link) {
					this.schoolService.editCourse(
						this.courseInEditing.id,
						this.courseInEditing.name,
						this.courseInEditing.description,
						response.link
					)
					.subscribe(data => {
						if(!data) return;
						this.bottomSheetRef.dismiss();
						this.messenger.emit('refreshLists');
						this.snackBar.show(`New course added :D`);
					});
				}
			});
		}
	}
	public deleteCourse(id: number): void {
		this.schoolService.deleteCourse(id).subscribe(
			data => {
				if(!data) return;
				this.bottomSheetRef.dismiss();
				this.messenger.emit('refreshLists');
				this.snackBar.show(`The course has been deleted :O`);
			}
		);
	}

	ngOnDestroy() {}
}
import { Component, OnInit } from '@angular/core';

import { SnackBarService } from '../../services/snack-bar.service';
import { SchoolService } from '../../services/school.service';
import { SchoolAuthService } from '../../services/school-auth.service';
import { Admin } from '../../models/admin';


@Component({
	selector: 'app-nav-bar',
	templateUrl: './nav-bar.component.html',
	styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
	public userInfo: Admin = {};
	constructor(
		private schoolService: SchoolService,
		private schoolAuthService: SchoolAuthService,
		private snackBar: SnackBarService
	) {}
	ngOnInit() {
		this.schoolService.getCurrentAdmin()
		.subscribe(data => this.userInfo = (data)? data : {});
	}

	public isLoggedIn(): boolean {
		return this.schoolAuthService.isLoggedIn();
	}
	public logout() {
		this.schoolAuthService.logout();
	}
}

/**
 * @function to - a function to run a function inside a promise and to
 * return an array of the result and error. to be used with await.
 * @param fn - any function that is expected to run inside of the promise
 * @param args - input arguments into fn
 * @returns - an array [data, error].
 */
export default function to(fn: any, ...args: any[]): Promise<any> {
	return Promise.resolve(fn(...args))
		.then(data => [data, undefined])
		.catch(error => [undefined, error]);
}

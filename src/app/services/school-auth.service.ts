import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap, retryWhen, delay, take } from 'rxjs/operators';

import { Admin } from '../models/admin';
import { LocalStorageService } from './local-storage.service';


@Injectable({ providedIn: 'root' }) export class SchoolAuthService {
	private readonly URL: string = '/api';
	private readonly httpOptions: object;
	constructor(
		private http: HttpClient,
		private storageService: LocalStorageService
	) {
		this.httpOptions = {
			headers: new HttpHeaders({
				'Access-Control-Allow-Origin': '*',
				'Content-Type': 'application/json',
				'Accept': 'application/json',
			})
		};
	}

	public login(email: string, password: string): Observable<any> {
		return this.http.post<any>(
			`${this.URL}/login`,
			{email, password},
			this.httpOptions
		).pipe(
			catchError(this.handleError<any>('login')),
			tap((data: any) => {
				if(data && data.access_token) console.log(`[SchoolService] Log in successful.`);
				else console.error(`[SchoolService] There was an error in logging you in.`);
			}),
			retryWhen(err => err.pipe(
				delay(2000), take(3)
			))
		);
	}
	isLoggedIn(): boolean {
		if(!this.storageService.get('currentUser')) return false;
		const userData = this.storageService.get('currentUser');
		if((userData.timestamp + (userData.expires_in * 60 * 1000)) > Date.now())
			return true;
		else {
			this.storageService.remove('currentUser');
			return false;
		}
	}
	public logout(): void {
		this.storageService.remove('currentUser');
		console.log(`[SchoolService] Log out successful.`);
	}

	public register(
		name: string,
		role: string,
		phone: string,
		email: string,
		imageURL: string,
		password: string
	): Observable<Admin> {
		return this.http.post<Admin>(
			`${this.URL}/register`,
			{name, role, phone, email, imageURL, password},
			this.httpOptions
		).pipe(
			catchError(this.handleError<any>('register')),
			tap((data: any) => {
				if(data && data.access_token) console.log(`[SchoolService] Registered successfully.`);
				else {
					console.error(`[SchoolService] There was an error in registering you.`);
					return;
				}
			}),
			retryWhen(err => err.pipe(
				delay(2000), take(3)
			)),
		);
	}

	/** @function handleError - Error handler */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			console.error(`[SchoolService.${operation}()] Error: ${error.message}`);
			return of(result as T);
		};
	}
}

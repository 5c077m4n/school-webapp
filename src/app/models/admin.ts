export interface Admin {
	id?: number;
	timestamp?: number;
	email?: string,
	name?: string;
	role?: string;
	phone?: string;
	password?: string;
	image_url?: string;
}

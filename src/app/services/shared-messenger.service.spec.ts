import { TestBed, inject } from '@angular/core/testing';

import { SharedMessengerService } from './shared-messenger.service';

describe('SharedMessengerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SharedMessengerService]
    });
  });

  it('should be created', inject([SharedMessengerService], (service: SharedMessengerService) => {
    expect(service).toBeTruthy();
  }));
});

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { SchoolAuthService } from '../services/school-auth.service';

@Injectable({ providedIn: 'root' }) export class IsLoggedInGuard implements CanActivate {
	constructor(private schoolAuthService: SchoolAuthService) {}

	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean> | Promise<boolean> | boolean {
		if(this.schoolAuthService.isLoggedIn()) return true;
		else return false;
	}
}

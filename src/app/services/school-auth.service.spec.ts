import { TestBed, inject } from '@angular/core/testing';

import { SchoolAuthService } from './school-auth.service';

describe('SchoolAuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SchoolAuthService]
    });
  });

  it('should be created', inject([SchoolAuthService], (service: SchoolAuthService) => {
    expect(service).toBeTruthy();
  }));
});

import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';


@Injectable({ providedIn: 'root' }) export class SnackBarService {
	private config: MatSnackBarConfig = new MatSnackBarConfig();
	constructor(private snackBar: MatSnackBar) {
		this.config.panelClass = ['snack-bar'];
		this.config.duration = 3000;
		this.config.horizontalPosition = 'center';
	}

	show(msg: string) {
		this.snackBar.open(msg, null, this.config);
	}
}

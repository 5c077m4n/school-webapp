import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { StylesModule } from './modules/styles/styles.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { RegisterComponent } from './components/register/register.component';
import { ImageUploadComponent } from './components/image-upload/image-upload.component';
import { SchoolMainComponent } from './components/school-main/school-main.component';
import { StudentListComponent, AddStudentBottomSheetComponent } from './components/student-list/student-list.component';
import { CourseListComponent, AddCourseBottomSheetComponent } from './components/course-list/course-list.component';
import { AdminMainComponent, AddAdminBottomSheetComponent } from './components/admin-main/admin-main.component';


@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		NavBarComponent,
		RegisterComponent,
		ImageUploadComponent,
		SchoolMainComponent,
		StudentListComponent,
		CourseListComponent,
		AdminMainComponent,
		AddStudentBottomSheetComponent,
		AddCourseBottomSheetComponent,
		AddAdminBottomSheetComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		FormsModule,
		StylesModule,
		AppRoutingModule,
	],
	exports: [],
	providers: [],
	bootstrap: [AppComponent],
	entryComponents: [
		AddStudentBottomSheetComponent,
		AddCourseBottomSheetComponent,
		AddAdminBottomSheetComponent
	],
})
export class AppModule {}

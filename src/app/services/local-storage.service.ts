import { Injectable } from '@angular/core';


@Injectable({ providedIn: 'root' }) export class LocalStorageService {
	constructor() {}

	add(key: string, value: any): void {
		window.localStorage.setItem(key, JSON.stringify(value));
	}
	get(key: string): any {
		return JSON.parse(window.localStorage.getItem(key));
	}
	remove(key: string): void {
		window.localStorage.removeItem(key);
	}

	clear(): void {
		window.localStorage.clear();
	}
}

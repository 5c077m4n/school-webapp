import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap, retryWhen, delay, take } from 'rxjs/operators';


@Injectable({ providedIn: 'root' }) export class ImageUploadService {
	private readonly URL: string = `/api/image-upload`;
	private readonly httpOptions;
	constructor(private http: HttpClient) {
		this.httpOptions = {
			headers: new HttpHeaders({
				'Access-Control-Allow-Origin': '*',
				'Content-Type': 'multipart/form-data',
				'Accept': 'application/json'
			})
		};
	}

	private async imgToBin(imageFile: File): Promise<Blob> {
		let imgBin;
		const imageReader = new FileReader();
		imageReader.onload = () => imgBin = imageReader.result;
		imageReader.onerror = () => console.error(imageReader.error);
		await imageReader.readAsBinaryString(imageFile);
		return imgBin;
	}

	public uploadImage(image: File): Observable<any> {
		const uploadData = new FormData();
		uploadData.append('image', image, image.name);
		uploadData.append('Content-Disposition', `form-data; name="image"; filename="${image.name}"`);
		uploadData.append('Content-Type', `image/${image.type}`);
		uploadData.append('Access-Control-Allow-Origin', '*');
		uploadData.append('Accept', 'application/json');

		return this.http.post<any>(this.URL, uploadData)
		.pipe(
			catchError(this.handleError<any>('uploadImage')),
			tap((data: any) => {
				if(data && data.success) console.log(`[ImageUploadService] Image uploaded successfully.`);
				else console.error(`[ImageUploadService] There was an error uploading your image.`);
			}),
			retryWhen(err => err.pipe(
				delay(5000), take(3)
			))
		);
	}

	/** @function handleError - returns a function for error handling */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			console.error(`[SchoolService.${operation}()] Error: ${error.message}`);
			return of(result as T);
		};
	}
}

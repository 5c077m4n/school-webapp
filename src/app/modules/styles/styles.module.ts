import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatBadgeModule } from '@angular/material/badge';
import { MatChipsModule } from '@angular/material/chips';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';


const materialModules = [
	BrowserAnimationsModule,
	MatButtonModule,
	MatInputModule,
	MatListModule,
	MatBadgeModule,
	MatChipsModule,
	MatToolbarModule,
	MatFormFieldModule,
	MatCardModule,
	MatTooltipModule,
	MatSelectModule,
	MatExpansionModule,
	MatSnackBarModule,
	FlexLayoutModule,
	MatBottomSheetModule,
	MatMenuModule,
	MatIconModule
];

@NgModule({
	imports: [
		CommonModule,
		...materialModules
	],
	exports: [
		...materialModules
	],
	declarations: []
})
export class StylesModule {}

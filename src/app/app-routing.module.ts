import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Route } from '@angular/router';

import { IsLoggedInGuard } from './guards/is-logged-in.guard';
import { RoleGuard } from './guards/role.guard';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { SchoolMainComponent } from './components/school-main/school-main.component';
import { AdminMainComponent } from './components/admin-main/admin-main.component';
import { StudentListComponent } from './components/student-list/student-list.component';
import { CourseListComponent } from './components/course-list/course-list.component';


const routes: Route[] = [
	{ path: '', component: SchoolMainComponent, pathMatch: 'full', canActivate: [
		IsLoggedInGuard
	] },
	{ path: 'login', component: LoginComponent, pathMatch: 'full' },
	{ path: 'register', component: RegisterComponent, pathMatch: 'full' },
	{ path: 'school', component: SchoolMainComponent, pathMatch: 'full', canActivate: [
		IsLoggedInGuard, RoleGuard
	] },
	{ path: 'student-list', component: StudentListComponent, pathMatch: 'full', canActivate: [
		IsLoggedInGuard
	] },
	{ path: 'course-list', component: CourseListComponent, pathMatch: 'full', canActivate: [
		IsLoggedInGuard, RoleGuard
	] },
	{ path: 'admin', component: AdminMainComponent, pathMatch: 'full', canActivate: [
		IsLoggedInGuard, RoleGuard
	] },
	{ path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forRoot(routes)
	],
	exports: [
		RouterModule
	],
	declarations: []
})
export class AppRoutingModule { }

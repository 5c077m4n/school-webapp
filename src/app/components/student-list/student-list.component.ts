import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material';

import { SchoolService } from '../../services/school.service';
import { SharedMessengerService } from '../../services/shared-messenger.service';
import { ImageUploadService } from '../../services/image-upload.service';
import { SnackBarService } from '../../services/snack-bar.service';
import { Student } from '../../models/student';


@Component({
	selector: 'app-student-list',
	templateUrl: './student-list.component.html',
	styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit, OnDestroy {
	public students: Student[];
	private listener: Subscription;
	constructor(
		private bottomSheet: MatBottomSheet,
		private schoolService: SchoolService,
		private messenger: SharedMessengerService
	) {}
	ngOnInit() {
		this.getAllStudents();
		this.listener = this.messenger.getObservable().subscribe(
			msg => (msg === 'refreshLists')? this.getAllStudents() : undefined
		);
	}
	public getAllStudents(): void {
		this.schoolService.getAllStudents().subscribe(
			response => this.students = (response)? response : []
		);
	}
	public openEditStudentPage(student: Student): void {
		this.openBottomSheet();
		this.messenger.emit(student);
	}

	private openBottomSheet(): void {
		this.bottomSheet.open(AddStudentBottomSheetComponent);
	}
	ngOnDestroy() {
		this.listener.unsubscribe();
	}
}

@Component({ templateUrl: './add-student-bottom-page.component.html' })
export class AddStudentBottomSheetComponent implements OnInit, OnDestroy {
	public studentInEditing: Student = {};
	public courses: any[] = [];
	private listener: Subscription;
	constructor(
		private bottomSheetRef: MatBottomSheetRef<AddStudentBottomSheetComponent>,
		private schoolService: SchoolService,
		private imageUploadService: ImageUploadService,
		private snackBar: SnackBarService,
		private messenger: SharedMessengerService
	) {}
	ngOnInit() {
		this.getAllCourses();
		this.listener = this.messenger.getObservable()
		.subscribe(msg => this.studentInEditing = (msg.courses)? msg : {});
	}
	private getAllCourses(): void {
		this.schoolService.getAllCourses().subscribe(
			response => {
				if(!response) return;
				else this.courses = response
					.map(course => course = Object.assign(course, {isChecked: false}));
			}
		);
	}
	private getSelectedCourses(): number[] {
		return this.courses
            .filter(course => course.isChecked)
            .map(course => course.id);
	}

	public addStudent(imageFile?: File): void {
		if(!this.studentInEditing.name.trim()) return;
		if(!imageFile) {
			this.schoolService.addStudent(
				this.studentInEditing.name.trim(),
				this.studentInEditing.phone,
				this.studentInEditing.email,
				null,
				this.getSelectedCourses()
			).subscribe(response => {
				if(!response) return;
				this.bottomSheetRef.dismiss();
				this.messenger.emit('refreshLists');
				this.snackBar.show(`New student added :D`);
			});
		}
		else {
			if(imageFile.size > 4e6) {
				this.snackBar.show('Please choose a smaller image (less than 4MB).');
				return;
			}
			this.imageUploadService.uploadImage(imageFile)
			.subscribe(response => {
				if(response && response.link) {
					this.schoolService
					.addStudent(
						this.studentInEditing.name.trim(),
						this.studentInEditing.phone,
						this.studentInEditing.email,
						response.link,
						this.getSelectedCourses()
					)
					.subscribe(data => {
						if(!response) return;
						this.bottomSheetRef.dismiss();
						this.messenger.emit('refreshLists');
						this.snackBar.show(`New student added :D`);
					});
				}
			});
		}
	}
	updateStudent(imageFile?: File): void {
		if(!this.studentInEditing.name.trim()) return;
		if(!imageFile) {
			this.schoolService.editStudent(
				this.studentInEditing.id,
				this.studentInEditing.name.trim(),
				this.studentInEditing.phone,
				this.studentInEditing.email,
				null,
				this.getSelectedCourses()
			).subscribe(data => {
				if(!data) return;
				this.bottomSheetRef.dismiss();
				this.messenger.emit('refreshLists');
				this.snackBar.show(`Student updated! :D`);
			});
		}
		else {
			if(imageFile.size > 4e6) {
				this.snackBar.show('Please choose a smaller image (less than 4MB).');
				return;
			}
			this.imageUploadService.uploadImage(imageFile)
			.subscribe(response => {
				if(response && response.link) {
					this.schoolService
					.editStudent(
						this.studentInEditing.id,
						this.studentInEditing.name.trim(),
						this.studentInEditing.phone,
						this.studentInEditing.email,
						response.link,
						this.getSelectedCourses()
					)
					.subscribe(data => {
						if(!data) return;
						this.bottomSheetRef.dismiss();
						this.messenger.emit('refreshLists');
						this.snackBar.show(`Student updated! :D`);
					});
				}
			});
		}

	}
	public deleteStudent(id: number): void {
		this.schoolService.deleteStudent(id).subscribe(
			data => {
				if(!data) return;
				this.bottomSheetRef.dismiss();
				this.messenger.emit('refreshLists');
				this.snackBar.show(`The student has been deleted :O`);
			}
		);
	}

	ngOnDestroy() {
		this.listener.unsubscribe();
	}
}

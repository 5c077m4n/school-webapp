import { Component, OnInit } from '@angular/core';
import { SchoolService } from '../../services/school.service';
import { Admin } from '../../models/admin';


@Component({
	selector: 'app-school-main',
	templateUrl: './school-main.component.html',
	styleUrls: ['./school-main.component.css']
})
export class SchoolMainComponent implements OnInit {
	public currentUser: Admin = {};
	constructor(private schoolService: SchoolService) {}
	ngOnInit() {
		this.getCurrentUser();
	}

	getCurrentUser() {
		this.schoolService.getCurrentAdmin().subscribe(
			data => this.currentUser = (data)? data : {}
		);
	}
}

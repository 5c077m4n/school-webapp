import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap, retryWhen, delay, take } from 'rxjs/operators';

import { LaravelResponse } from '../models/laravel-response';
import { Student } from '../models/student';
import { Course } from '../models/course';
import { Admin } from '../models/admin';
import { LocalStorageService } from './local-storage.service';


@Injectable({ providedIn: 'root' }) export class SchoolService {
	private readonly URL: string = '/api';
	private readonly httpOptions;
	constructor(
		private http: HttpClient,
		private storageService: LocalStorageService
	) {
		if(window.localStorage.getItem('currentUser')) {
			this.httpOptions = {
				headers: new HttpHeaders({
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${this.storageService.get('currentUser').access_token}`
				})
			};
		}
		else this.httpOptions = {
			headers: new HttpHeaders({ 'Content-Type': 'application/json' })
		};
	}

	/** Testing if the server is available */
	public connectionTest(): Observable<any> {
		return this.http.get<any>(this.URL).pipe(
			tap(response =>
				(response)? response : console.error(`[SchoolService] The server is unavailable.`)
			),
			catchError(this.handleError<any>('connectionTest'))
		);
	}
	public getCurrentAdmin(): Observable<Admin> {
		return this.http.get(`${this.URL}/current-user`, this.httpOptions).pipe(
			catchError(this.handleError<any>('getCurrentAdmin')),
			tap(data => {
				if(!data) console.log(`[SchoolService] There was an error in getting your info.`);
			}),
			retryWhen(err => err.pipe(
				delay(2000), take(3)
			))
		);
	}

	public getAllAdmins(): Observable<Admin[]> {
		return this.http.get<Admin[]>(`${this.URL}/admins`, this.httpOptions).pipe(
			catchError(this.handleError<any>('getAllAdmins')),
			tap(data => {
				if(!data) console.log(`[SchoolService] There was an error in getting the admins.`);
				return data;
			}),
			retryWhen(err => err.pipe(
				delay(2000), take(3)
			))
		);
	}
	public addAdmin(
		name: string,
		role: string,
		phone: string,
		email: string,
		image_url: string,
		password: string
	): Observable<Admin> {
		return this.http.post<Admin>(
			`${this.URL}/admins`,
			{name, role, phone, email, image_url, password},
			this.httpOptions
		).pipe(
			tap((data: any) => {
				if(data) console.log(`[SchoolService] Admin added.`);
				else console.error(`[SchoolService] There was an error in adding the admin.`);
			}),
			catchError(this.handleError<any>('addAdmin'))
		);
	}
	public editAdmin(id: number, ...args: any[]): Observable<LaravelResponse<Admin>> {
		return this.http.put<LaravelResponse<Admin>>(
			`${this.URL}/admins/${id}`,
			{...args},
			this.httpOptions
		).pipe(
			tap((data: any) => {
				if(data) console.log(`[SchoolService] Admin edited.`);
				else console.error(`[SchoolService] There was an error in editing the admin.`);
			}),
			catchError(this.handleError<any>('editAdmin'))
		);
	}
	deleteAdmin(id: number): Observable<LaravelResponse<Admin>> {
		return this.http.delete<LaravelResponse<Admin>>(
			`${this.URL}/admins/${id}`,
			this.httpOptions
		).pipe(
			tap((data: any) => {
				if(data && data.success) console.log(`[SchoolService] Admin deleted.`);
				else console.error(`[SchoolService] There was an error in deleting the admin.`);
			}),
			catchError(this.handleError<any>('deleteAdmin'))
		);
	}

	/** @method GET functions for students */
	public getAllStudents(): Observable<Student[]> {
		return this.http.get<Student[]>(
			`${this.URL}/students`,
			this.httpOptions
		).pipe(
			tap(response => {
				if(response) console.log(`[SchoolService] Got all students.`);
				else console.error(`[SchoolService] There was an error in getting the students.`);
			}),
			catchError(this.handleError<any>('getAllStudents'))
		);
	}
	public getStudent(id: number): Observable<Student> {
		return this.http.get<Student>(
			`${this.URL}/students/${id}`,
			this.httpOptions
		).pipe(
			tap((data: any) => {
				if(data) console.log(`[SchoolService] Got student #${id}.`);
				else console.error(`[SchoolService] There was an error in getting the student.`);
			}),
			catchError(this.handleError<any>('getStudent'))
		);
	}
	/** @method POST function for students */
	public addStudent(name: string,	phone: string, email: string, image_url: string, courses: number[]): Observable<LaravelResponse<Student>> {
		return this.http.post<LaravelResponse<Student>>(
			`${this.URL}/students`,
			{name, phone, email, image_url, courses},
			this.httpOptions
		).pipe(
			tap((data: any) => {
				if(data) console.log(`[SchoolService] Added student.`);
				else console.error(`[SchoolService] There was an error in adding the student.`);
			}),
			catchError(this.handleError<any>('addStudent'))
		);
	}
	/** @method PUT function for students */
	public editStudent(id: number, ...args: any[]): Observable<LaravelResponse<Student>> {
		return this.http.put<LaravelResponse<Student>>(
			`${this.URL}/students/${id}`,
			{...args},
			this.httpOptions
		).pipe(
			tap((data: any) => {
				if(data) console.log(`[SchoolService] Edited the student.`);
				else console.error(`[SchoolService] There was an error in editing the student.`);
			}),
			catchError(this.handleError<any>('editStudent'))
		);
	}
	/** @method DELETE function for students */
	public deleteStudent(id: number): Observable<LaravelResponse<Student>> {
		return this.http.delete<LaravelResponse<Student>>(
			`${this.URL}/students/${id}`,
			this.httpOptions
		).pipe(
			tap((data: any) => {
				if(data) console.log(`[SchoolService] Deleted the student.`);
				else console.error(`[SchoolService] There was an error in deleting the student.`);
			}),
			catchError(this.handleError<any>('deleteStudent'))
		);
	}

	/** @method GET function for courses */
	public getAllCourses(): Observable<Course[]> {
		return this.http.get<LaravelResponse<Course[]>>(
			`${this.URL}/courses`,
			this.httpOptions
		).pipe(
			tap((data: any) => {
				if(data) console.log(`[SchoolService] Got all courses.`);
				else console.error(`[SchoolService] There was an error in getting the courses.`);
			}),
			catchError(this.handleError<any>('getAllCourses'))
		);
	}
	public getAllStudentsOfCourse(id: number): Observable<LaravelResponse<Student[]>> {
		return this.http.get<LaravelResponse<Student[]>>(
			`${this.URL}/courses/${id}/students`,
			this.httpOptions
		).pipe(
			tap((data: any) => {
				if(data) console.log(`[SchoolService] Got all courses.`);
				else console.error(`[SchoolService] There was an error in getting the courses.`);
			}),
			catchError(this.handleError<any>('getAllStudentsOfCourse'))
		);
	}
	public getCourse(id: number): Observable<LaravelResponse<Course>> {
		return this.http.get<LaravelResponse<Course>>(
			`${this.URL}/courses/${id}`,
			this.httpOptions
		).pipe(
			tap((data: any) => {
				if(data) console.log(`[SchoolService] Got all courses.`);
				else console.error(`[SchoolService] There was an error in getting the courses.`);
			}),
			catchError(this.handleError<any>('getCourse'))
		);
	}
	/** @method POST function for courses */
	public addCourse(name: string, description: string, image_url: string): Observable<LaravelResponse<Course>> {
		return this.http.post<LaravelResponse<Course>>(
			`${this.URL}/courses`,
			{name, description, image_url},
			this.httpOptions
		).pipe(
			tap((data: any) => {
				if(data) console.log(`[SchoolService] Added the course.`);
				else console.error(`[SchoolService] There was an error in adding the course.`);
			}),
			catchError(this.handleError<any>('addCourse'))
		);
	}
	/** @method PUT function for courses */
	public editCourse(id: number, ...args: any[]): Observable<LaravelResponse<Course>> {
		return this.http.put<LaravelResponse<Course>>(
			`${this.URL}/courses/${id}`,
			{...args},
			this.httpOptions
		).pipe(
			tap((data: any) => {
				if(data) console.log(`[SchoolService] Edited the course.`);
				else console.error(`[SchoolService] There was an error in editing the course.`);
			}),
			catchError(this.handleError<any>('editCourse'))
		);
	}
	/** @method DELETE function for courses */
	public deleteCourse(id: number): Observable<LaravelResponse<Course>> {
		return this.http.delete<LaravelResponse<Course>>(
			`${this.URL}/courses/${id}`,
			this.httpOptions
		).pipe(
			tap((data: any) => {
				if(data) console.log(`[SchoolService] Deleted the course.`);
				else console.error(`[SchoolService] There was an error in deleting the course.`);
			}),
			catchError(this.handleError<any>('deleteCourse'))
		);
	}

	/** @function handleError - Error handler */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			console.error(`[SchoolService.${operation}()] Error: ${error.message}`);
			return of(result as T);
		};
	}
}

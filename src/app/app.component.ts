import { Component } from '@angular/core';

import { SchoolAuthService } from './services/school-auth.service';


@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	constructor(private schoolAuthService: SchoolAuthService) {}

	public isLoggedIn(): boolean {
		return this.schoolAuthService.isLoggedIn();
	}
}

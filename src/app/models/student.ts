import { Course } from "./course";


export interface Student {
	id?: number;
	name?: string;
	phone?: string;
	email?: string;
	image_url?: string;
	courses?: Course[];
}

import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';


@Injectable({ providedIn: 'root' }) export class SharedMessengerService {
	private subject: Subject<any> = new Subject<any>();
	constructor() {}

	public emit(msg: any): void {
		this.subject.next(msg);
    }
    public clear(): void {
        this.subject.next();
    }
    public getObservable(): Observable<any> {
        return this.subject.asObservable();
    }
}

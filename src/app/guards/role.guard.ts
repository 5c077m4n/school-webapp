import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { SchoolService } from '../services/school.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
	constructor(private schoolService: SchoolService) {}

	async canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Promise<boolean> {
		let isManager: boolean;
		await this.schoolService.getCurrentAdmin().toPromise()
			.then(data => isManager = ((data.role === 'manager') || (data.role === 'owner')))
			.catch(_ => isManager = false);
		return isManager;
	}
}
